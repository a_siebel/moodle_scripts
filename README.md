# Moodle Skripte

Dieses Repository enthält verschiedene Moodle-Skripte. Diese sind für das Hesssiche Bildungslayout entworfen, welches auf dem Theme Boost basiert.

## Skripte

### Moodle vereinfachen (moodle_vereinfachen_block.html)

Blendet einen Button ein, der beim Erstellen von Inhalten verschiedene Optionen ausblendet, die weniger oft genutzt werden.

### MERUEF Blog (meruef_block.html)

Gestaltet das Layout im Sinne des [MERUEF-Prinzips]([IM³ Kursstruktur - HackMD](https://hackmd.io/IcqrCklfR0yhjs_H32scPw)) um.

![Vorschau](./images/meruef.png)

### Startbutton ersetzen (sph_replace_start.html)

Ersetzt den Link des Startbutton, so dass dieser auf die Moodle-Startseite (anstatt auf paedorg) verweist.

### Startbutton umbenennen (sph_rename_start.html)

Benennt den Start-Button in SPH Start und Startseite in Schulmoodle Startseite um


### Verstecke Skripte (hide_Script_blocks.html)

Versteckt Skripte in Blöcken. Dieses Skript muss nach allen Blöcken (z.B. als letzter Block) ausgeführt werden.

### style.scss / Layoutanpassungen für das hessische Schulmoodle.

Umfangreiche Layoutanpassungen, die das Layout stärker an das Layout vom Schulportal anpassen.

### sph_courses_as_list

Stellt Kurse im SPH Layout als Liste dar.

![Vorschau](./images/courses_as_list.png)

### style.scss / Layoutanpassungen für das hessische Schulmoodle.

Umfangreiche Layoutanpassungen, die das Layout stärker an das Layout vom Schulportal anpassen.


## Anleitung

Du kannst die Skripte entweder

  a) Als Administrator unter  additionalhtml hinzufügen oder 
  
  b) als Block. Wenn du einen Block auf der Startseite hinzufügst, kannst du ihn auch auf jeder Seite anzeigen lassen. 




